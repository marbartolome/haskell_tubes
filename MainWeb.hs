{-# LANGUAGE OverloadedStrings #-}

import Tubes
import Web.Scotty
import Data.Monoid (mconcat)

main = scotty 3000 $ do
  get "/" $ do
    origin <- param "origin"
    destiny <- param "destiny"
    let stations = readStations $ origin++","++destiny
    json ( map (unescapeOutput . show) $ shortestPath (stations !! 0) (stations !! 1))
    
  get "/list" $ do
    json (map (unescapeOutput . show) $ listStations)

  notFound $ do
    text "404"
