-- Tube lines and stations are sourced from: https://github.com/mental/lib2geom/blob/d41250ddef88ac67683061d60c3759528c074364/src/2geom/toys/data/london.txt

module Tubes where

import Data.List (find, intersperse)


-- Contains all valid station identifiers.
data Station = Acton_Town | Aldgate | Aldgate_East | Alperton | Angel | Archway | Arnos_Grove | Arsenal | Baker_Street | Balham | Bank | Barbican | Barking | Barkingside | Barons_Court | Bayswater | Becontree | Belsize_Park | Bent_Cross | Bermondsey | Bethnal_Green | Blackfriars | Blackhorse_Road | Bond_Street | Borough | Boston_Manor | Bounds_Green | Bow_Road | Brixton | Bromley_by_Bow | Buckhurst_Hill | Burnt_Oak | Caledonian_Road | Camden_Town | Canada_Water | Canary_Wharf | Canning_Town | Cannon_Street | Canons_Park | Chalk_Farm | Chancery_Lane | Charing_Cross | Chigwell | Chiswick_Park | Clapham_Common | Clapham_North | Clapham_South | Cockfosters | Colindale | Colliers_Wood | Covent_Garden | Croxley | Dagenham_East | Dagenham_Heathway | Debden | Dollis_Hill | Ealing_Broadway | Ealing_Common | Earl's_Court | East_Acton | Eastcote | East_Finchley | East_Ham | East_Putney | Edgware | Edgware_Road | Elephant_and_Castle | Elm_Park | Embankment | Epping | Euston | Euston_Square | Fairlop | Farringdon | Finchley_Central | Finchley_Road | Finsbury_Park | Fulham_Broadway | Gants_Hill | Gloucester_Road | Golders_Green | Goldhawk_Road | Goodge_Street | Grange_Hill | Great_Portland_Street | Greenford | Green_Park | Gunnersbury | Hainault | Hammersmith | Hampstead | Hanger_Lane | Harlesden | Harrow_on_the_Hill | Harrow_and_Wealdstone | Hatton_Cross | Heathrow_Terminal_4 | Heathrow_Terminals_1_2_3 | Hendon_Central | High_Barnet | Highbury_and_Islington | Highgate | High_Street_Kensington | Hillingdon | Holborn | Holland_Park | Holloway_Road | Hornchurch | Hounslow_Central | Hounslow_East | Hounslow_West | Hyde_Park_Corner | Ickenham | Kennington | Kensal_Green | Kensington_Olympia | Kentish_Town | Kenton | Kew_Gardens | Kilburn | Kilburn_Park | Kingsbury | King's_Cross_St_Pancras | Knightsbridge | Ladbroke_Grove | Lambeth_North | Lancaster_Gate | Latimer_Road | Leicester_Square | Leyton | Leytonstone | Liverpool_Street | London_Bridge | Loughton | Maida_Vale | Manor_House | Mansion_House | Marble_Arch | Marylebone | Mile_End | Mill_Hill_East | Monument | Moorgate | Moor_Park | Morden | Mornington_Crescent | Neasden | Newbury_Park | New_Cross | New_Cross_Gate | North_Acton | North_Ealing | Northfields | North_Greenwich | North_Harrow | Northolt | North_Wembley | Northwick_Park | Northwood | Northwood_Hills | Notting_Hill_Gate | Oakwood | Old_Street | Osterley | Oval | Oxford_Circus | Paddington | Park_Royal | Parsons_Green | Perivale | Piccadilly_Circus | Pimlico | Pinner | Plaistow | Preston_Road | Putney_Bridge | Queensbury | Queen's_Park | Queensway | Ravenscourt_Park | Rayners_Lane | Redbridge | Regent's_Park | Richmond | Roding_Valley | Rotherhithe | Royal_Oak | Ruislip | Ruislip_Gardens | Ruislip_Manor | Russell_Square | Seven_Sisters | Shadwell | Shepherd's_Bush | Shoreditch | Sloane_Square | Snaresbrook | South_Ealing | Southfields | Southgate | South_Harrow | South_Kensington | South_Kenton | South_Ruislip | Southwark | South_Wimbledon | South_Woodford | Stamford_Brook | Stanmore | Stepney_Green | St_James's_Park | St_John's_Wood | Stockwell | Stonebridge_Park | St_Paul's | Stratford | Sudbury_Hill | Sudbury_Town | Surrey_Quays | Swiss_Cottage | Temple | Theydon_Bois | Tooting_Bec | Tooting_Broadway | Tottenham_Court_Road | Tottenham_Hale | Totteridge_and_Whetstone | Tower_Hill | Tufnell_Park | Turnham_Green | Turnpike_Lane | Upminster | Upminster_Bridge | Upney | Upton_Park | Uxbridge | Vauxhall | Victoria | Walthamstow_Central | Wanstead | Wapping | Warren_Street | Warwick_Avenue | Waterloo | Watford | Wembley_Central | Wembley_Park | West_Acton | Westbourne_Park | West_Brompton | West_Finchley | West_Ham | West_Hampstead | West_Harrow | West_Kensington | Westminster | West_Ruislip | Whitechapel | White_City | Willesden_Green | Willesden_Junction | Wimbledon | Wimbledon_Park | Woodford | Wood_Green | Woodside_Park deriving (Show, Eq, Read, Enum) 

type LineName = String

-- A node is a station in the context of a line
type Node = (LineName, Station)

-- A path is a sequence of nodes.
-- It is stored backwards for performance.
type Path = [Node]

-- A line describes the sequence of stations, and holds a name
type Line = (LineName, [Station])

-- Tube Lines
bakerlooLine = ("Bakerloo",[Harrow_and_Wealdstone,Kenton,South_Kenton,North_Wembley,Wembley_Central,Stonebridge_Park,Harlesden,Willesden_Junction,Kensal_Green,Queen's_Park,Kilburn_Park,Maida_Vale,Warwick_Avenue,Paddington,Edgware_Road,Marylebone,Baker_Street,Regent's_Park,Oxford_Circus,Piccadilly_Circus,Charing_Cross,Embankment,Waterloo,Lambeth_North,Elephant_and_Castle])

centralLine = ("Central",[West_Ruislip,Ruislip_Gardens,South_Ruislip,Northolt,Greenford,Perivale,Hanger_Lane,North_Acton,East_Acton,White_City,Shepherd's_Bush,Holland_Park,Notting_Hill_Gate,Queensway,Lancaster_Gate,Marble_Arch,Bond_Street,Oxford_Circus,Tottenham_Court_Road,Holborn,Chancery_Lane,St_Paul's,Bank,Liverpool_Street,Bethnal_Green,Mile_End,Stratford,Leyton,Leytonstone,Snaresbrook,South_Woodford,Woodford,Buckhurst_Hill,Loughton,Debden,Theydon_Bois,Epping])

circleLine = ("Circle",[Hammersmith,Goldhawk_Road,Shepherd's_Bush,Latimer_Road,Ladbroke_Grove,Westbourne_Park,Royal_Oak,Paddington,Edgware_Road,Baker_Street,Great_Portland_Street,Euston_Square,King's_Cross_St_Pancras,Farringdon,Barbican,Moorgate,Liverpool_Street,Aldgate,Tower_Hill,Monument,Cannon_Street,Mansion_House,Blackfriars,Temple,Embankment,Westminster,St_James's_Park,Victoria,Sloane_Square,South_Kensington,Gloucester_Road,High_Street_Kensington,Notting_Hill_Gate,Bayswater,Paddington,Edgware_Road])

hammersmithLine = ("Hammersmith & City",[Hammersmith,Goldhawk_Road,Shepherd's_Bush,Latimer_Road,Ladbroke_Grove,Westbourne_Park,Royal_Oak,Paddington,Edgware_Road,Baker_Street,Great_Portland_Street,Euston_Square,King's_Cross_St_Pancras,Farringdon,Barbican,Moorgate,Liverpool_Street,Aldgate_East,Whitechapel,Stepney_Green,Mile_End,Bow_Road,Bromley_by_Bow,West_Ham,Plaistow,Upton_Park,East_Ham,Barking])

districtLine1 = ("District 1",[Ealing_Broadway,Ealing_Common,Acton_Town,Chiswick_Park,Turnham_Green,Stamford_Brook,Ravenscourt_Park,Hammersmith,Barons_Court,West_Kensington,Earl's_Court,Gloucester_Road,South_Kensington,Sloane_Square,Victoria,St_James's_Park,Westminster,Embankment,Temple,Blackfriars,Mansion_House,Cannon_Street,Monument,Tower_Hill,Aldgate_East,Whitechapel,Stepney_Green,Mile_End,Bow_Road,Bromley_by_Bow,West_Ham,Plaistow,Upton_Park,East_Ham,Barking,Upney,Becontree,Dagenham_Heathway,Dagenham_East,Elm_Park,Hornchurch,Upminster_Bridge,Upminster])

districtLine2 = ("District 2",[Kensington_Olympia,Earl's_Court,High_Street_Kensington,Notting_Hill_Gate,Bayswater,Paddington,Edgware_Road])

overgroundLine = ("Overground",[Shoreditch,Whitechapel,Shadwell,Wapping,Rotherhithe,Canada_Water,Surrey_Quays,New_Cross])

jubileeLine = ("Jubilee",[Stratford,West_Ham,Canning_Town,North_Greenwich,Canary_Wharf,Canada_Water,Bermondsey,London_Bridge,Southwark,Waterloo,Westminster,Green_Park,Bond_Street,Baker_Street,St_John's_Wood,Swiss_Cottage,Finchley_Road,West_Hampstead,Kilburn,Willesden_Green,Dollis_Hill,Neasden,Wembley_Park,Kingsbury,Queensbury,Canons_Park,Stanmore])

metropolitanLine = ("Metropolitan",[Moor_Park,Northwood,Northwood_Hills,Pinner,North_Harrow,Harrow_on_the_Hill,Northwick_Park,Preston_Road,Wembley_Park,Finchley_Road,Baker_Street,Great_Portland_Street,Euston_Square,King's_Cross_St_Pancras,Farringdon,Barbican,Moorgate,Liverpool_Street,Aldgate])

northernLine1 = ("Northern 1",[High_Barnet,Totteridge_and_Whetstone,Woodside_Park,West_Finchley,Finchley_Central,East_Finchley,Highgate,Archway,Tufnell_Park,Kentish_Town,Camden_Town,Euston,King's_Cross_St_Pancras,Angel,Old_Street,Moorgate,Bank,London_Bridge,Borough,Elephant_and_Castle,Kennington,Oval,Stockwell,Clapham_North,Clapham_Common,Clapham_South,Balham,Tooting_Bec,Tooting_Broadway,Colliers_Wood,South_Wimbledon,Morden])

northernLine2 = ("Northern 2",[Mill_Hill_East,Finchley_Central,East_Finchley,Highgate,Archway,Tufnell_Park,Kentish_Town,Camden_Town,Mornington_Crescent,Euston,Warren_Street,Goodge_Street,Tottenham_Court_Road,Leicester_Square,Charing_Cross,Embankment,Waterloo,Kennington,Oval,Stockwell,Clapham_North,Clapham_Common,Clapham_South,Balham,Tooting_Bec,Tooting_Broadway,Colliers_Wood,South_Wimbledon,Morden])

piccadillyLine = ("Piccadilly",[Uxbridge,Hillingdon,Ickenham,Ruislip,Ruislip_Manor,Eastcote,Rayners_Lane,South_Harrow,Sudbury_Hill,Sudbury_Town,Alperton,Park_Royal,North_Ealing,Ealing_Common,Acton_Town,Turnham_Green,Hammersmith,Barons_Court,Earl's_Court,Gloucester_Road,South_Kensington,Knightsbridge,Hyde_Park_Corner,Green_Park,Piccadilly_Circus,Leicester_Square,Covent_Garden,Holborn,Russell_Square,King's_Cross_St_Pancras,Caledonian_Road,Holloway_Road,Arsenal,Finsbury_Park,Manor_House,Turnpike_Lane,Wood_Green,Bounds_Green,Arnos_Grove,Southgate,Oakwood,Cockfosters])

victoriaLine = ("Victoria",[Walthamstow_Central,Blackhorse_Road,Tottenham_Hale,Seven_Sisters,Finsbury_Park,Highbury_and_Islington,King's_Cross_St_Pancras,Euston,Warren_Street,Oxford_Circus,Green_Park,Victoria,Pimlico,Vauxhall,Stockwell,Brixton])

waterlooLine = ("Waterloo & City",[Waterloo,Bank])

tubelines = [bakerlooLine, centralLine, circleLine, hammersmithLine, districtLine1, districtLine2, overgroundLine, jubileeLine, metropolitanLine, northernLine1, northernLine2, piccadillyLine, victoriaLine, waterlooLine]

-- Represents a direct connection between two nodes.
-- It is undirected, the order of the nodes should not matter.
-- So Link A B is the same as Link B A.
data Link = Link Node Node deriving (Show, Eq) 

-- The tube network can be represented as a graph
-- (stored as a collection of links)
type Network = [Link]

-- Returns the set of links between stations
-- in a line.
getSameLineLinks :: Line -> [Link]
getSameLineLinks line = 
    let stations = snd line
        name = fst line
    in map (\(stat1, stat2) ->Link (name, stat1) (name, stat2)) $ zip stations (tail stations)

-- Given a station identifier, returns all nodes
-- of that station present in all lines in the network
getStationNodes :: Station-> [Node]
getStationNodes station = [ (name, station) | (name, stations) <- tubelines, station `elem` stations]

-- Make links between all unrepeated combinations of node pairs.
stationNodesToLinks :: [Node] -> [Link]
stationNodesToLinks nodes = 
   let allpairs = [(a,b) | a<-nodes, b<-nodes, a/=b]
       uniquepairs = foldl (\acc (a,b) -> if not $ (b,a) `elem` acc then (a,b):acc else acc) [] allpairs
   in [Link a b | (a,b)<-uniquepairs]

-- Returns a collection of nodes from stations in a line
-- between the same station in the other lines.
getInterLineLinks :: Line -> [Link] 
getInterLineLinks line = concat $ map (stationNodesToLinks . getStationNodes) (snd line)

-- Given a collection of links, removes the duplicates.
-- Two links are duplicate if they refer to the same nodes 
-- no matter the order of the nodes.
removeDuplicateLinks::Network->Network
removeDuplicateLinks network = foldl (\acc (Link a b) -> if ((Link a b) `elem` acc) || ((Link b a) `elem` acc) then acc else (Link a b):acc) [] network

-- Builds a tube network from a list of lines
-- It will calculate all of the nodes and links between them.
buildNetwork :: [Line] -> Network
buildNetwork tubemap = removeDuplicateLinks $ concatMap (\line -> (getSameLineLinks line) ++ (getInterLineLinks line)) tubelines

-- Build a tube network from our stations
netmap :: Network
netmap = buildNetwork tubelines

-- Given a station in the network, 
-- returns a list of the stations
-- directly connected to it.
getDirectCons :: Node -> [Node]
getDirectCons station = [ getLinkDestiny station (Link a b) | Link a b <-netmap, station==a || station==b] 

-- Same as before but no need to provide the network
getDirectCons' station = getDirectCons station

-- Given a station and a link (containing such station)
-- return the linked station.
-- This will only be called when we know our station is
-- on the link, so no need to consider the case when otherwise.
getLinkDestiny :: Node -> Link -> Node
getLinkDestiny station (Link a b) = if station==a then b else a

-- Progresses a path by advancing a step in all possible directions.
-- It is possible that multiple paths can surge
-- from a single path if we are at a node connected
-- to multiple others.
-- Avoids nodes already visited in the path.
pathStep :: Path -> [Path]
pathStep (last:rest) = [linked:last:rest | linked<- getDirectCons' last, not (linked `elem` last:rest)]

-- Same as pathStep, but for multiple paths
multiPathStep :: [Path] -> [Path]
multiPathStep paths = foldl (\acc path -> (pathStep path)++acc) [] paths

-- If a path starts and ends at the selected stations,
-- hooray! it's a solution
isSolution :: Station -> Station -> Path -> Bool
isSolution _ _ [] = False
isSolution orig dest path = ((snd $ head path) == dest) && ((snd $ last path) == orig)

findSolution :: [Path] -> Station -> Station -> Maybe Path
findSolution paths orig dest = find (isSolution orig dest) paths

-- Helper function for shortestPath that uses
-- an accumulator to hold the up to date list
-- of potential paths (until it finds a valid one).
shortestPath' :: Station -> Station -> [Path] -> Path
shortestPath' orig dest [] = shortestPath' orig dest (map (\node -> [node]) $(getStationNodes orig))
shortestPath' orig dest potentialpaths= 
     if potentialsol /= Nothing then (\(Just x) -> x)potentialsol
     else shortestPath' orig dest updatedpotentialpaths
     where 
        potentialsol = findSolution updatedpotentialpaths orig dest
        updatedpotentialpaths = multiPathStep potentialpaths 

-- Given a sequence of stations, removes consecutive duplicates.
removeDupeStations :: [Station] -> [Station]
removeDupeStations [] = []
removeDupeStations (s1:[]) = [s1]
removeDupeStations (s1:s2:stations) = if s1==s2 then s1:(removeDupeStations stations) else s1:(removeDupeStations (s2:stations))

-- Turns a path to a list of the station it traverses.
-- If there are line changes (in the same station) the 
-- station is only listed once in the resulting list.
pathToStationList :: Path -> [Station]
pathToStationList path = removeDupeStations $ map snd path

-- Gives you the shortest path between two stations.
-- It reverses the order of the path, so that it reads
-- normally.
shortestPath :: Station -> Station -> [Station]
shortestPath orig dest = 
    if orig==dest then [orig]    
    else reverse $ pathToStationList $ shortestPath' orig dest []

--- IO functions

-- Returns a Just element if the read was successful,
-- otherwise returns a Nothing
readMaybe :: (Read a) => String -> Maybe a
readMaybe st = case reads st of [(x, "")] -> Just x
                                _ -> Nothing

-- True if a maybe is Just, False if it's Nothing
isSomething :: (Eq a) => Maybe a -> Bool
isSomething a = if a==Nothing then False else True

-- Transforms a string containing comma separated
-- station names into a list of Stations. Stations
-- are wrapped as Maybe elements, so that if a 
-- station name doesn't match with any existing 
-- stations, it will be matched to Nothing on the list.
readMaybeStations :: String -> [Maybe Station]
readMaybeStations str = map readMaybe (splitCommas str)

-- Replaces spaces in input for _
escapeInput :: String -> String
escapeInput str = map (\x -> if x==' ' then '_' else x) str

-- Replaces _ in output for spaces
unescapeOutput :: String -> String
unescapeOutput str = map (\x -> if x=='_' then ' ' else x) str

-- Transforms a string containing comma separated
-- station names into a list of Stations.
-- Emits erros if a invalid list of Stations is read.
readStations :: String -> [Station]
readStations str  
	     | Nothing `elem` maybeStats = error "Inexistent station. Enter 'list' to show all available."
         | length maybeStats /= 2 = error "Must provide exactly two stations."
         | otherwise = map (\(Just x) -> x) maybeStats
         where maybeStats = readMaybeStations $ escapeInput str

-- Splits a string by commas
splitCommas :: String -> [String]
splitCommas "" = [] 
splitCommas str = 
    let (x,s) = span (/=',') str -- unintended kirby 
    in x : ( splitCommas (if length s > 0 then tail s else s))

-- Turns a path into a comman separated string
-- of path names.
pathToString :: [Station] -> String
pathToString [] = ""
pathToString path = concat $ intersperse "," $ map (unescapeOutput . show) path

-- Returns the full list of valid stations
listStations :: [Station]
listStations = [Acton_Town .. Woodside_Park]




