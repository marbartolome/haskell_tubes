import Tubes

main = do
  putStrLn "Enter origin and destiny stations (example: Origin,Destiny). Or 'list' to see all stations: " 
  line <- getLine
  if (line == "list") 
  then do 
      putStrLn $ pathToString $ listStations
      main
  else do 
      let stations = readStations line
          path = shortestPath (stations !! 0) (stations !! 1)
      putStrLn $ "Shortest path: "++pathToString path
